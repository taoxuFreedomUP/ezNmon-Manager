package tdd.xuchun.test.service;

import java.util.Map;

import tdd.xuchun.test.model.Host;
import tdd.xuchun.test.model.Page;

/**
 * 主机接口服务
 * 
 * @author ZSL
 *
 */
public interface IHostService {
	/**
	 * 查询单个主机详细信息
	 * 
	 * @param host
	 * @return
	 */
	public Host getHost(Host host);
	
	/**
	 * 查询单个主机详细信息利用IP
	 * 
	 * @param host
	 * @return
	 */
	public Host getHostByIP(Host host);
	
	/**
	 * 增加新主机
	 * 
	 * @param host
	 */
	public void addHost(Host host);
	/**
	 * 删除主机
	 * 
	 * @param host
	 */
	public void delHost(Host host);
	/**
	 * 修改主机
	 * 
	 * @param host
	 */
	public void editHost( Host host);
	/**
	 * 模糊查询匹配主机列表
	 * 
	 * @param cond
	 * @return
	 */
	public Page queryLikeHosts(Map<String,Object> cond);
}
