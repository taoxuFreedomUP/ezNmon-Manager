﻿<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>EasyNmon管理平台</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/layui.css" media="all">
<script src="${request.contextPath}/js/vue/vue.min.js"></script>
<script src="${request.contextPath}/js/jquery/jquery.min.js"></script>
<script src="${request.contextPath}/js/laypage/laypage.js" charset="utf-8"></script>
<script src="${request.contextPath}/js/layer/layer.js" charset="utf-8"></script>
<script src="${request.contextPath}/js/bootstrap/bootstrap.min.js"></script>
<script src="${request.contextPath}/js/layui/layui.js"></script>
<body>
	<div id="app" class="container">
		<div class="page-header">
			<h3>EasyNmon管理平台   </h3>  
		</div>
		<div class="panel-body">
			<form class="form-inline bg-info" role="form">
				查询条件：
				<div class="form-group">
					<label class="sr-only" for="name">主机名称</label> <input type="text"
						class="form-control" id="name" placeholder="主机名称">
					<label class="sr-only" for="addr">主机IP</label> <input type="text"
						class="form-control" id="addr" placeholder="主机IP">
					<label class="sr-only" for="remark">备注</label> <input type="text"
						class="form-control" id="remark" placeholder="备注">
				</div>
				<div class="btn-group">
					<button type="button" id="findHost" class="btn btn-success">
						<span class="glyphicon glyphicon-search"></span> 查询主机
					</button>
					<button type="button" id="addHostBtn" class="btn btn-primary">
						<span class="glyphicon glyphicon-plus"></span> 增加主机
					</button>
					<button type="button" id="importHostBtn" class="btn btn-danger" >
						<span class="glyphicon glyphicon-upload" ></span> 导入主机
					</button>
					<button type="button" id="downloadtempleBtn" class="btn btn-info"  onclick="downloadTemplate()">
						<span class="glyphicon glyphicon-download"></span> 下载模板
					</button>
					
				</div>
			</form>
			<br>
			<form class="form-inline bg-warning" role="form">
				覆盖安装
				<div class="form-group">
					<input type="checkbox" name="cover" id="cover" class="form-control" />
				</div>
				<div class="btn-group">
					<button type="button" id="setupandRun" class="btn btn-danger"
						onclick="setUpServer()">
						<span class="glyphicon glyphicon-send"></span> 安装服务
					</button>
					<button type="button" onclick="clearservice()"
							class="btn btn-primary">
						<span class="glyphicon glyphicon-trash"></span> 卸载服务
					</button>
				</div>

				服务监控：
				<div class="form-group">
					<label class="sr-only" for="name">监控频度（秒）</label> <input
						type="text" class="form-control" id="f" value=""
						placeholder="监控频度（秒）">
				</div>
				<div class="form-group">
					<label class="sr-only" for="addr">监控时长（分钟）</label> <input
						type="text" class="form-control" id="t" value=""
						placeholder="监控时长（分钟）">
				</div>
				<div class="btn-group">
					<button type="button" id="startMoniter" onclick="startEasyNmon()"
						class="btn btn-success">
						<span class="glyphicon glyphicon-play"></span> 开始监控
					</button>
					<button type="button" id="stopMoniter"
						onclick="stopEasyNmon()"
						class="btn btn-warning">
						<span class="glyphicon glyphicon-stop"></span> 停止监控
					</button>
					<button type="button" onclick="clearReport()"
						class="btn btn-danger">
						<span class="glyphicon glyphicon-remove-circle"></span> 清空报告
					</button>

				</div>
			</form>
		</div>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr class="success">
						<td><input type="checkbox" name="ids" id="ids"
							onchange="chgAll(this)" /></td>
						<td><b>序号</b></td>
						<td><b>名称</b></td>
						<td><b>地址</b></td>
						<td><b>备注</b></td>
						<td><b>监控端口</b></td>
						<td><b>监控状态</b></td>
						<td><b>操作 </b></td>
					</tr>
				</thead>
				<tbody>
					<tr class="active" v-for="(item,index) in result">
						<td><input type="checkbox" name="id" :value="item.id"
							onchange="chg(this)"></td>
						<td>{{index+1}}</td>
						<td :id="'name_'+item.id"><a data-toggle="tooltip"
							title="查看报告" @click="showConsole(item.addr,item.eznmonport)">{{item.name}}</a></td>
						<td>{{item.addr}}</td>
						<td>{{item.remark}}</td>
						<td>{{item.eznmonport}}</td>
						<td :id="'stat_'+item.id"></td>
						<td><a @click="editEvent(item.id)" data-toggle="tooltip"
							title="编辑"> <span class="glyphicon glyphicon-edit"></span></a> <a
							data-toggle="tooltip" title="删除" @click="delEvent(item.id)"><span
								class="glyphicon glyphicon-trash"></span></a> <a
							data-toggle="tooltip" title="复制新增" @click="copyAddEvent(item.id)"><span
								class="glyphicon glyphicon-plus"></span></a> <a
							data-toggle="tooltip" title="下载报告" @click="getReport(item.id)"><span
								class="glyphicon glyphicon-download-alt"></span></a></td>
					</tr>
				</tbody>
			</table>
			<div id="pagenav"></div><a href="https://gitee.com/goodhal/ezNmon-Manager" data-toggle="tooltip" target="_blank" >更多信息</a> 
			
			
		</div>
		
	</div>

	<script>
		function getContextPath(){
		    return "${request.contextPath}";
		}
		var contextPath = getContextPath();
		var app = new Vue({
			el : '#app',
			data : {
				result : []
			}
		});
		layui.use('upload', function(){
			var upload = layui.upload;

			//执行上传
			var uploadInst = upload.render({
				elem: '#importHostBtn' //绑定元素
				,url: '/host/importTemplate' //上传接口
				,method: 'POST'
				,accept: 'file'
				,size: 50
				,before: function(obj){
					layer.load();
				}
				,done: function(res){//上传完毕回调
					layer.closeAll('loading');
					getHostPageList();
					layer.alert(res.msg);
				}
				,error: function(){//请求异常回调
					layer.closeAll('loading');
					getHostPageList();
					layer.msg('网络异常，请稍后重试！');
				}
			});
		});

		//查询主机数据
		var getHostPageList = function(curr) {
			$.ajax({
				type : "GET",
				dataType : "json",
				url : contextPath+"/host/getcond",
				data : {
					pageNum : curr || 1,
					pageSize : 10,
					name : $("#name").val(),
					addr : $("#addr").val(),
					remark : $("#remark").val()
				//向服务端传的参数，此处只是演示
				},
				success : function(msg) {
					app.result = msg.result;
					laypage({
						cont : 'pagenav', //容器。值支持id名、原生dom对象，jquery对象,
						pages : msg.totalPage, //总页数
						first : true,
						last : true,
						curr : curr || 1, //当前页
						jump : function(obj, first) { //触发分页后的回调
							if (!first) { //点击跳页触发函数自身，并传递当前页：obj.curr
								getHostPageList(obj.curr);
							}
						}
					});
				}
			});
		}
		getHostPageList();
		//查询主机事件
		$('#findHost').on('click', function() {
			getHostPageList();
		});

		//增加主机事件
		$('#addHostBtn').on('click', function() {
			layer.open({
				type : 2,
				title : '添加主机',
				fix : false,
				maxmin : true,
				shadeClose : true,
				area : [ '1100px', '600px' ],
				content : contextPath+'/host/host_add',
				end : function() {
					getHostPageList();
				}
			});

		});
		//查看报告
		var showConsole = function(addr, port) {
			var url = 'http://' + addr + ":" + port + "/report";
			//var tempWin = window.open(url, 'height=400, width=400, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');
			var tempWin = window.open(url, "_blank");
			$.ajax({
				url : url,
				type : 'GET',
				cache: false,
				dataType: "jsonp", //跨域采用jsonp方式 
				processData: false,
				timeout:2000, //超时时间，毫秒
				complete : function(response) {
					//alert(response.status);
					if (response.status == 200) {
						tempWin.location.href=url;
					} else {
						tempWin.close();
						layer.alert("无法访问");
					}
				}
			});
		}
		//编辑主机事件
		var editEvent = function(id) {
			layer.open({
				type : 2,
				title : '编辑主机',
				fix : false,
				maxmin : true,
				shadeClose : true,
				area : [ '1100px', '600px' ],
				content : contextPath+'/host/editpage?id=' + id,
				end : function() {
					getHostPageList();
				}
			});
		}

		//复制新增主机事件
		var copyAddEvent = function(id) {
			layer.open({
				type : 2,
				title : '复制新增主机',
				fix : false,
				maxmin : true,
				shadeClose : true,
				area : [ '1100px', '600px' ],
				content : contextPath+'/host/copyaddpage?id=' + id,
				end : function() {
					getHostPageList();
				}
			});
		}
		//下载报告
		var getReport = function(id) {
			var tempWin = window.open("about:blank"); // window.open("about:blank"); 
			$.ajax({
				type : "GET",
				dataType : "json",
				url : contextPath+"/easynmon/getreport",
				data : {
					id : id
				},
				success : function(msg) {
					var mes = msg.message;
					if (mes.indexOf('http') < 0) {
						layer.alert(mes);
					} else {
						tempWin.location.href = mes;

					}
				}
			});

		}
		//下载机器导入模板
		var downloadTemplate = function() {
			var tempWin = window.open("about:blank"); // window.open("about:blank");
			tempWin.location.href = contextPath+"/host/downloadTemplate";
		}

		//删除事件
		var delEvent = function(id) {
			//询问框
			layer.confirm('您确定要删除么？', {
				btn : [ '是', '否' ]
			//按钮
			}, function() {
				//是
				$.ajax({
					type : "GET",
					dataType : "json",
					url : contextPath+"/host/del",
					data : {
						id : id
					},
					success : function(msg) {
						getHostPageList();
						layer.msg('已经成功删除记录' + id, {
							icon : 5
						});
					}
				});

			}, function() {
				//否
			});
		}
		function setUpServer() {
			if (document.getElementById("cover").checked) {
				goEasyNmon(contextPath+'/easynmon/setupandruncover');
			} else {
				goEasyNmon(contextPath+'/easynmon/setupandrun');
			}

		}

		function clearReport() {
			//询问框
			layer.confirm('您确定要清空报告么？', {
				btn : [ '是', '否' ]
			//按钮
			}, function() {
				//是
				goEasyNmon(contextPath+'/easynmon/clearreport');
				layer.close(layer.index);

			}, function() {
				layer.close(layer.index);
				//否
			});
		}
		function clearservice() {
			//询问框
			layer.confirm('您确定要卸载服务吗？', {
				btn : [ '是', '否' ]
				//按钮
			}, function() {
				//是
				goEasyNmon(contextPath+'/easynmon/clearservice');
				layer.close(layer.index);

			}, function() {
				layer.close(layer.index);
				//否
			});
		}
		function chgAll(t) {//第一个复选框选中或取消选中，则下面的复选框为全选或取消全选；
			var ids = $.makeArray($("input[name='id']"));
			for ( var i in ids) {
				ids[i].checked = t.checked;
			}
		}
		function chg(t) {//当下面的复选框全部选中时，则将第一个复选框设置为选中，当下面的复选框中有一个没有被选中时，则第一个复选框取消选中；
			document.getElementById("ids").checked = true;
			var ids = $.makeArray($("input[name='id']"));
			for ( var i in ids) {
				if (ids[i].checked == false) {//如果所有的复选框只要有一个未选中，则第一个复选框不会选中
					document.getElementById("ids").checked = false;
					return;
				}

			}
		}
		//安装easyNmon
		function startEasyNmon() {//将下面的复选框的id值传递给Controller层，组成id数组，拼接url到controller层，调用批量删除方法（deleteBatch())方法
			var ids = $.makeArray($("input[name='id']"));
			var url = contextPath+'/easynmon/startmoniter';//此url指向controller层的setupandrun方法，需要id属性
			var flag = true;
			var tv = document.getElementById("t").value;
			var fv = document.getElementById("f").value;
			if (fv == '' || tv == '') {
				layer.alert('监控时长和监控频度不能为空！');
				return;
			}
			url += "?t=" + tv + "&f=" + fv;
			for ( var i in ids) {//遍历数组
				//alert(ids[i].value);
				if (ids[i].checked == true) {
					flag = false;
					ajaxurl(url + "&id=" + ids[i].value);//把拼接好的id数组传给页面
				}
			}
			if (flag) {//如果没有选中
				layer.alert("请选中主机！");
				return;
			}

		}
		function stopEasyNmon(){
			goEasyNmon(contextPath+'/easynmon/stopmoniter');
		}
		
		//easyNmon服务
		function goEasyNmon(myurl) {//将下面的复选框的id值传递给Controller层，组成id数组，拼接url到controller层，调用批量删除方法（deleteBatch())方法
			var ids = $.makeArray($("input[name='id']"));
			var url = myurl;//此url指向controller层的setupandrun方法，需要id属性
			var flag = true;
			for ( var i in ids) {//遍历数组
				//alert(ids[i].value);
				if (ids[i].checked == true) {
					ajaxurl(url + "?id=" + ids[i].value);//第一个id属性前加？拼接
					flag = false;
				}
			}
			if (flag) {//如果没有选中
				layer.alert("请选中主机！");
				return;
			}

		}

		var ajaxurl = function(myurl) {
			$.ajax({
				type : "GET",
				dataType : "json",
				url : myurl,
				data : {},
				success : function(msg) {
					layer.alert(msg.message);
					layer.zIndex=layer.zIndex+1;
				}
			});
		}
		function getEasyNmonState() {
			var ids = $.makeArray($("input[name='id']"));
			for ( var i in ids) {
				var url = contextPath+"/easynmon/geteasynmonstate?id=" + ids[i].value;//第一个id属性前加？拼接
				$
						.ajax({
							type : "GET",
							dataType : "json",
							url : url,
							data : {},
							success : function(msg) {
								//alert(msg.message);
								var pair = msg.message.split("_");
								id = pair[0];
								code = pair[1];
								name = pair[2];
								if (code == 200) {
									document.getElementById('stat_' + id).innerHTML = '<span class="label label-success">服务正常</span>';
								} else {
									document.getElementById('stat_' + id).innerHTML = '<span class="label label-warning">无法访问</span>';
								}

							}
						});
			}

		}
		getEasyNmonState();

		$(document).ready(function() {
			var timer = setInterval(function() {
				getEasyNmonState()
			}, 2000);
		});
		
		
	</script>
</body>
</html>